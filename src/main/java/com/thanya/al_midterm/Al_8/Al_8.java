/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.al_midterm.Al_8;

import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author Thanya
 */
public class Al_8 {
    public static void main(String[] args) {
        Scanner kb= new Scanner(new InputStreamReader(System.in));
        int n= kb.nextInt();
        int arr[] = new int[n];
        
        for(int i=0; i<arr.length; i++){
            arr[i] = kb.nextInt();
        }
        reverseArr(arr);
    }
    
    public static int[] reverseArr(int[] arr) {
        int[] temp = new int[arr.length];
        for (int i=0; i<arr.length; i++){
            temp[arr.length -1 -i] = arr[i];
        }
        for(int i=0; i<arr.length; i++){
            arr[i] = temp[i];
        }
        for(int i=0; i<arr.length; i++){
            System.out.print(arr[i] + " ");
        }
        return arr;
    }
}
