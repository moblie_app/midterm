/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.al_midterm.MaxsubArray;

import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author Thanya
 */
public class Maxsub {
    public static void main(String[] args) {
        Scanner kb = new Scanner(new InputStreamReader(System.in));
        int n = kb.nextInt();
        int arr[] = new int[n];
        
        for(int i=0; i<arr.length; i++){
            arr[i] = kb.nextInt();
        }
        System.out.println("Maximum Subarray sum of array arr is "+maxSub(arr));
    }
    
    public static int maxSub(int[] arr) {
        int max = Integer.MIN_VALUE;
        
        for(int i=0; i<arr.length-1; i++) {
            int sum = 0;
            for(int j=i; j<=arr.length-1; j++) {
                sum += arr[j];
                if (sum > max) {
                    max = sum;
                }
            }
        }
        return max;
    }
}
